import threading
import time
import pyaudio
import wave


class AudioRecorder(threading.Thread):
    """Audio class based on pyAudio and Wave"""
    def __init__(self, name="temp_audio", folder='.', rate=44100, fpb=11025, channels=1, rec_time=10.0):
        self._rate = rate
        self._frames_per_buffer = fpb
        self._channels = channels
        self._format = pyaudio.paInt16
        self._filename = name + '.wav'
        self._folder = folder
        self._audio = pyaudio.PyAudio()
        self._stream = self._audio.open(format=self._format,
                                        channels=self._channels,
                                        rate=self._rate,
                                        input=True,
                                        frames_per_buffer=self._frames_per_buffer)
        self._audio_frames = []
        self._start_time = time.time()
        self._rec_time = rec_time
        self._return = None

        threading.Thread.__init__(self, name=name)

    def run(self):
        """Audio starts being recorded"""
        t = self._start_time
        record_until_t = t + self._rec_time
        self._stream.start_stream()
        while time.time() < record_until_t:  # and self.open
            data = self._stream.read(self._frames_per_buffer)
            self._audio_frames.append(data)

        self.stop()
        self._return = {'result': 'OK'}
        return

    def stop(self):
        """Finishes the _audio recording therefore the thread too"""
        self._stream.stop_stream()
        self._stream.close()
        self._audio.terminate()
        wave_file = wave.open(f'{self._folder}{self._filename}', 'wb')
        wave_file.setnchannels(self._channels)
        wave_file.setsampwidth(self._audio.get_sample_size(self._format))
        wave_file.setframerate(self._rate)
        wave_file.writeframes(b''.join(self._audio_frames))
        wave_file.close()

    def join(self, timeout=None):
        threading.Thread.join(self, timeout)
        return self._return
