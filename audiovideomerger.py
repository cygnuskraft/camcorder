import threading
import subprocess
import os
from pathlib import Path


class AudioVideoMerger(threading.Thread):

    def __init__(self, name, folder=None):
        self._name = name
        self._folder = folder
        self._return = None

        threading.Thread.__init__(self, name=name)

    def run(self):

        # if name not provided, create name from files in folder that start with _temp_
        if self._name == 'None':
            g = Path(self._folder).rglob('_temp_V_*')
            files = [str(x).replace(self._folder, '') for x in g]
            if files:
                self._name = files[0].split('.')[0]
            else:
                self._return = {'result': 'START'}
                return

        video = f'{self._folder}{self._name}.mp4'
        audio_wav = f'{self._folder}{self._name}.wav'
        audio_mp3 = f'{self._folder}{self._name}.mp3'
        video_out = video.replace('_temp_', '')

        # create .mp3 from .wav & delete .wav
        cmd = f'ffmpeg -i {audio_wav} -acodec libmp3lame {audio_mp3}'
        subprocess.call(cmd, shell=True)
        os.remove(audio_wav)

        # merge .mp3 and .mp4 in new mp4 & delete original files
        cmd = f'ffmpeg -i {audio_mp3} -i {video} {video_out}'  # -ac 2 -channel_layout mono
        subprocess.call(cmd, shell=True)
        os.remove(video)
        os.remove(audio_mp3)

        self._return = {'result': 'OK'}

    def join(self, timeout=None):
        threading.Thread.join(self, timeout)
        return self._return
