import threading
import time
import datetime
import cv2


class VideoRecorder(threading.Thread):
    """Video class based on openCV"""
    def __init__(self, name="temp_video", folder='.', cam=0, sizex=1280, sizey=720, fps=3.0,
                 timestamp=True, scale=1.0, rec_time=10.0):
        self.open = True
        self._filename = name + '.mp4'
        self._folder = folder
        self._cam = cam
        self._sizex = sizex
        self._sizey = sizey
        self._fps = fps
        self._timestamp = timestamp
        self._scale = scale
        self._scaledx = int(self._sizex * self._scale)
        self._scaledy = int(self._sizey * self._scale)
        self._frame_counts = 0
        self._start_time = None
        self._rec_time = rec_time
        self._return = None

        self._capture = None
        self._fourcc = None
        self._writer = None

        threading.Thread.__init__(self)

    def run(self):
        """Video starts being recorded"""
        self._start_time = time.time()
        self._capture = cv2.VideoCapture(self._cam)
        self._capture.set(cv2.CAP_PROP_FRAME_WIDTH, self._sizex)
        self._capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self._sizey)
        self._fourcc = cv2.VideoWriter_fourcc(*'mp4v')  # (*'mp4v'(mp4) 'avc1'(h.264) 'hvc1'(h.265)
        self._writer = cv2.VideoWriter(f'{self._folder}{self._filename}', self._fourcc,
                                       self._fps, (self._scaledx, self._scaledy))

        t = self._start_time
        record_until_t = self._start_time + self._rec_time
        interval = 1 / self._fps

        while self._capture.isOpened() and self.open and time.time() < record_until_t:
            ret, frame = self._capture.read()
            if self._frame_counts == 0:
                print(f'EFFECTIVE_RESOLUTION: {len(frame[0])}x{len(frame)}')
            if ret:
                # scale the frame
                if self._scale != 1:
                    frame = cv2.resize(frame, None, fx=self._scale, fy=self._scale, interpolation=cv2.INTER_AREA)
                # timestamped the frame
                if self._timestamp:
                    dt = datetime.datetime.now().strftime("%d-%b-%Y %H:%M:%S").upper()
                    frame = cv2.putText(frame, dt,
                                        (10, int(self._scaledy * 0.97)),  # posicion
                                        cv2.FONT_ITALIC, self._scale * 1.5,  # font & size
                                        (255, 255, 255),  # color font
                                        1, cv2.LINE_4)  # grosor
                self._writer.write(frame)
                self._frame_counts += 1
                t2 = t + interval
                time.sleep(max(t2 - time.time(), 0))
                t = t2
            else:
                self._writer.release()
                self._capture.release()
                self._return = {'result': 'Error'}
                return

        self._writer.release()
        self._capture.release()
        self._return = {'result': 'OK'}
        return

    def join(self, timeout=None):
        threading.Thread.join(self, timeout)
        return self._return
