from audiovideorecorder import AudioVideoRecorder


if __name__ == '__main__':
    av_rec = AudioVideoRecorder(cam=0, sizex=1280, sizey=720, scale=0.5, fps=3, timestamped=True,
                                rate=12000, fpb=4000, channels=1,
                                daily_files=96, folder='/Users/marcelotroitino/Desktop/videos/')
    av_rec.start()

