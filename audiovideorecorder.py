import os
import datetime
import math

from audiorecorder import AudioRecorder
from videorecorder import VideoRecorder
from audiovideomerger import AudioVideoMerger


class AudioVideoRecorder:

    def __init__(self, cam=0, sizex=1280, sizey=720, scale=0.5, fps=3, timestamped=True,
                 rate=12000, fpb=4000, channels=1,
                 daily_files=24, folder='.'):

        # Video Variables
        self._v_cam = cam
        self._v_sizex = sizex
        self._v_sizey = sizey
        self._v_scale = scale
        self._v_fps = fps
        self._v_timestamped = timestamped
        self._v_recorder = None

        # Audio Variables
        self._a_rate = rate
        self._a_fpb = fpb
        self._a_channels = channels
        self._a_recorder = None

        # AV Variables
        self._av_daily_files = daily_files
        self._av_folder = folder
        os.makedirs(self._av_folder, exist_ok=True)
        self._av_merger = None

    def start(self):
        """Start the AV Recording"""
        # Create (starting) instance of AudioVideoMerger
        self._av_merger = AudioVideoMerger(name='None', folder=self._av_folder)
        self._av_merger.start()

        while True:

            # set the temporary _audio and video filenames
            start_time = datetime.datetime.now()
            filename = start_time.strftime("_temp_V_%Y-%m-%d_%H-%M-%S")

            # calculate for how long we will record
            secs_today = (start_time - start_time.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
            video_index = (secs_today / 86400) * self._av_daily_files
            next_video_index = math.ceil(video_index)
            rec_time = (next_video_index - video_index) * 86400 / self._av_daily_files - (1 / self._v_fps)

            # Create instances for the Audio and Video recorders, start them, and wait for them to end
            self._v_recorder = VideoRecorder(name=filename,
                                             folder=self._av_folder,
                                             cam=self._v_cam,
                                             sizex=self._v_sizex,
                                             sizey=self._v_sizey,
                                             fps=self._v_fps,
                                             timestamp=self._v_timestamped,
                                             scale=self._v_scale,
                                             rec_time=rec_time)
            self._a_recorder = AudioRecorder(name=filename,
                                             folder=self._av_folder,
                                             rate=self._a_rate,
                                             fpb=self._a_fpb,
                                             channels=self._a_channels,
                                             rec_time=rec_time)
            self._v_recorder.start()
            self._a_recorder.start()

            self._av_merger.join()
            self._v_recorder.join()
            self._a_recorder.join()

            # Create instance of AudioVideoMerger
            self._av_merger = AudioVideoMerger(name=f'{filename}', folder=self._av_folder)
            self._av_merger.start()


